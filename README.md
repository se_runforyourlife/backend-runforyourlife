# Run for your life Backend Server

An api server for SE Project

-----

## Setting up API Server

### Prerequisites

* Install Node version 8.6 or greater
* Install PostgreSQL

### Project configuration

1. Install Project Dependencies

```sh
npm install
```

2. Setting up server environment variable

```sh
# Copy .env.example to .env and edit values as desired
cp .env.example .env
```

3. Setting up database

```sh
# Copy ormconfig.yml.example to ormconfig.yml
cp ormconfig.yml.example ormconfig.yml
```

example for database configuration

```yaml
default:
  type: 'postgres'
  host: 'localhost'
  port: 5432
  username: 'username' # Username Here
  password: 'password' # Password Here
  database: 'database' # Database Here
  synchronize: true
  logging: true
  entities:
    - 'dist/model/entity/**/*.js'
  migrations:
    - 'dist/model/migration/**/*.js'
  subscribers:
    - 'dist/model/subscriber/**/*.js'

```

### Running the server up

```sh
# To start server
npm run start

# To build server
npm run build

# To run unit test
npm run test

# To run and watch for test
npm run watch

# To format code
npm run format
```
