FROM node:11-alpine
WORKDIR /app
COPY . /app
RUN npm install && npm run build
ENTRYPOINT ["node", "dist/app.js"]