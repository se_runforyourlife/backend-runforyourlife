require('dotenv').config();

import 'reflect-metadata';
import Koa from 'koa';
import logger from 'koa-logger';
import cors from '@koa/cors';
import koaBody from 'koa-body';
import { createConnection } from 'typeorm';
import { PORT } from './config';
import serve from 'koa-static';
import { join } from 'path';

const main = async () => {
  await createConnection();
  const app = new Koa();
  app.use(serve(join(__dirname, '..', 'public'), { defer: true }));
  const router = await import('./router');
  app.use(logger());
  app.use(cors());
  app.use(koaBody({ multipart: true }));
  app.use(router.default.routes());

  app.listen(PORT, () => console.log(`Server started at port ${PORT}.`));
};

main().catch(error => console.error(error));
