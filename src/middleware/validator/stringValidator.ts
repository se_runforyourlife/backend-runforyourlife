import { IMiddleware } from 'koa-router';

interface StringValidatorOption {
  minLength?: number;
  maxLength?: number;
  pattern?: RegExp;
  isOptional?: boolean;
}

type StringValidator = (
  field: string,
  option?: StringValidatorOption,
  place?: 'body' | 'query' | 'params',
) => IMiddleware;

const validate = (
  field: string,
  value: string,
  option: StringValidatorOption,
) => {
  const { minLength, maxLength, pattern, isOptional } = option;
  if (value === undefined) {
    if (isOptional) {
      return value;
    }
    throw new Error(`${field} is undefined.`);
  }
  const parsedValue = typeof value === 'string' ? value : String(value);
  if (minLength !== undefined && parsedValue.length < minLength) {
    throw new Error(`${field}'s length is lower than ${minLength}.`);
  }
  if (maxLength !== undefined && parsedValue.length > maxLength) {
    throw new Error(`${field}'s length is higher than ${maxLength}.`);
  }
  if (pattern !== undefined && !pattern.test(parsedValue)) {
    throw new Error(`${field} does not match with the pattern.`);
  }
  return parsedValue;
};

/**
 * A function that return validation middleware
 * This validator will validate incoming string
 * If validation is failed, an error will be thrown and context status will be 400
 * @param field name of field that need to be validated
 * @param option an option to config validator
 * @param place where the field is, can be 'body', 'query' or 'params'
 */
const stringValidator: StringValidator = (
  field,
  option = {},
  place = 'body',
) => async (ctx, next) => {
  try {
    switch (place) {
      case 'body':
        ctx.request.body[field] = validate(
          field,
          ctx.request.body[field],
          option,
        );
        break;
      case 'params':
        ctx.params[field] = validate(field, ctx.params[field], option);
        break;
      case 'query':
        ctx.request.query[field] = validate(
          field,
          ctx.request.query[field],
          option,
        );
        break;
    }
    await next();
  } catch (error) {
    ctx.body = { message: error.toString() };
    ctx.status = 400;
    return;
  }
};

export default stringValidator;
