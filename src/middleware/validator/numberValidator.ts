import { IMiddleware } from 'koa-router';

interface NumberValidatorOption {
  min?: number;
  max?: number;
  isOptional?: boolean;
}

type NumberValidator = (
  field: string,
  option?: NumberValidatorOption,
  place?: 'body' | 'query' | 'params',
) => IMiddleware;

const numberRegex = /^[0-9]*$/;

const validate = (
  field: string,
  value: string,
  option: NumberValidatorOption,
) => {
  const { min, max, isOptional } = option;
  if (value === undefined) {
    if (isOptional) {
      return value;
    }
    throw new Error(`${field} is undefined.`);
  }
  if (typeof value === 'string' && !numberRegex.test(value)) {
    throw new Error(`${field} is not a number.`);
  } else if (typeof value !== 'string' && typeof value !== 'number') {
    throw new Error(`${field} is not a number.`);
  }
  const parsedValue = Number(value);
  if (min !== undefined && parsedValue < min) {
    throw new Error(`${field} is lower than ${min}.`);
  }
  if (max !== undefined && parsedValue > max) {
    throw new Error(`${field} is higher than ${max}.`);
  }
  return parsedValue;
};

/**
 * A function that return validation middleware
 * This validator will validate incoming number
 * If validation is failed, an error will be thrown and context status will be 400
 * @param field name of field that need to be validated
 * @param option an option to config validator
 * @param place where the field is, can be 'body', 'query' or 'params'
 */
const numberValidator: NumberValidator = (
  field,
  option = {},
  place = 'body',
) => async (ctx, next) => {
  try {
    switch (place) {
      case 'body':
        ctx.request.body[field] = validate(
          field,
          ctx.request.body[field],
          option,
        );
        break;
      case 'params':
        ctx.params[field] = validate(field, ctx.params[field], option);
        break;
      case 'query':
        ctx.request.query[field] = validate(
          field,
          ctx.request.query[field],
          option,
        );
        break;
    }
    await next();
  } catch (error) {
    ctx.body = { message: error.toString() };
    ctx.status = 400;
    return;
  }
};

export default numberValidator;
