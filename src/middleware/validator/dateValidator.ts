import { IMiddleware } from 'koa-router';

interface DateValidatorOption {
  isMillisecond?: boolean;
  isOptional?: boolean;
}

type DateValidator = (
  field: string,
  option?: DateValidatorOption,
  place?: 'body' | 'query' | 'params',
) => IMiddleware;

const numberRegex = /^[0-9]*$/;

const isoDateRegex = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d{3}Z$/;

const validate = (field: string, value: any, option: DateValidatorOption) => {
  const { isMillisecond, isOptional } = option;
  if (value === undefined) {
    if (isOptional) {
      return value;
    }
    throw new Error(`${field} is undefined.`);
  }
  if (typeof value === 'string') {
    if (isMillisecond && numberRegex.test(value)) {
      return new Date(value);
    }
    if (isoDateRegex.test(value)) {
      return new Date(value);
    }
    throw new Error(`${field} does not match date pattern.`);
  } else if (value instanceof Date) {
    return value;
  } else {
    throw new Error(`${field} is not string or Date.`);
  }
};

/**
 * A function that return validation middleware
 * This validator will validate incoming Date
 * Date string has to be ISOString format
 * If validation is failed, an error will be thrown and context status will be 400
 * @param field name of field that need to be validated
 * @param option an option to config validator
 * @param place where the field is, can be 'body', 'query' or 'params'
 */
const dateValidator: DateValidator = (
  field,
  option = {},
  place = 'body',
) => async (ctx, next) => {
  try {
    switch (place) {
      case 'body':
        ctx.request.body[field] = validate(
          field,
          ctx.request.body[field],
          option,
        );
        break;
      case 'params':
        ctx.params[field] = validate(field, ctx.params[field], option);
        break;
      case 'query':
        ctx.request.query[field] = validate(
          field,
          ctx.request.query[field],
          option,
        );
        break;
    }
    await next();
  } catch (error) {
    ctx.body = { message: error.toString() };
    ctx.status = 400;
    return;
  }
};

export default dateValidator;
