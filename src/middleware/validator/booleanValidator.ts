import { IMiddleware } from 'koa-router';

interface BooleanValidatorOption {
  isOptional?: boolean;
}

type BooleanValidator = (
  field: string,
  option?: BooleanValidatorOption,
  place?: 'body' | 'query' | 'params',
) => IMiddleware;

const validate = (
  field: string,
  value: string,
  option: BooleanValidatorOption,
) => {
  const { isOptional } = option;
  if (value === undefined) {
    if (isOptional) {
      return value;
    }
    throw new Error(`${field} is undefined.`);
  }
  if (typeof value === 'string') {
    return value.toLowerCase() === 'true';
  }
  if (typeof value === 'boolean') {
    return value;
  }
  throw new Error(`${field} is not string or boolean`);
};

/**
 * A function that return validation middleware
 * This validator will validate incoming boolean
 * If validation is failed, an error will be thrown and context status will be 400
 * @param field name of field that need to be validated
 * @param option an option to config validator
 * @param place where the field is, can be 'body', 'query' or 'params'
 */
const booleanValidator: BooleanValidator = (
  field,
  option = {},
  place = 'body',
) => async (ctx, next) => {
  try {
    switch (place) {
      case 'body':
        ctx.request.body[field] = validate(
          field,
          ctx.request.body[field],
          option,
        );
        break;
      case 'params':
        ctx.params[field] = validate(field, ctx.params[field], option);
        break;
      case 'query':
        ctx.request.query[field] = validate(
          field,
          ctx.request.query[field],
          option,
        );
        break;
    }
    await next();
  } catch (error) {
    ctx.body = { message: error.toString() };
    ctx.status = 400;
    return;
  }
};

export default booleanValidator;
