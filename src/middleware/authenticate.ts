import { Context } from 'koa';
import { IMiddleware } from 'koa-router';
import { Role } from '../type/user-type';
import { JwtModel } from '../model/JwtModel';

type AuthenticateMiddleware = (roles?: Role[]) => IMiddleware;

const authenticate: AuthenticateMiddleware = (
  roles = [Role.EventManager, Role.Runner, Role.Admin],
) => async (ctx: Context, next) => {
  try {
    if (!ctx.header['authorization']) {
      throw new Error('There is no authorized token.');
    }
    const [bearer, token] = ctx.header['authorization'].split(' ');
    if (bearer !== 'Bearer') {
      throw new Error('The token is not a bearer token.');
    }
    const { id, role } = JwtModel.verifyJwt(token);
    if (roles.includes(role)) {
      ctx.user = { id, role };
      await next();
    } else {
      throw new Error(`User's role is unauthorized.`);
    }
  } catch (error) {
    ctx.body = { message: error.toString() };
    ctx.status = 401;
  }
  return;
};

export default authenticate;
