import Router from 'koa-router';
import SearchController from './controller/search-controller';
import RootController from './controller/root-controller';
import UserController from './controller/user-controller';
import AuthenticateController from './controller/authenticate-controller';
import AnotherController from './controller/another-controller';
import EventController from './controller/event-controller';
import UploadController from './controller/upload-controller';
import TransactionController from './controller/transaction-controller';
import ReportController from './controller/report-controller';
import { API_BASE } from './config';

const router = new Router({ prefix: API_BASE });

new RootController().mountToRouter(router);
new AnotherController().mountToRouter(router);
new SearchController().mountToRouter(router);
new UserController().mountToRouter(router);
new AuthenticateController().mountToRouter(router);
new SearchController().mountToRouter(router);
new EventController().mountToRouter(router);
new UploadController().mountToRouter(router);
new TransactionController().mountToRouter(router);
new ReportController().mountToRouter(router);

export default router;
