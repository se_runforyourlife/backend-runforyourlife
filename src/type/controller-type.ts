import { IMiddleware } from 'koa-router';

export type RouteMethod = 'get' | 'post' | 'put' | 'patch' | 'delete';

export interface Route {
  method: RouteMethod;
  path: string;
  handler: IMiddleware;
  middlewares?: IMiddleware[];
}

export interface Event {
  name: String;
  type: String;
  date: Date;
  isRegister: Boolean;
  location: String;
  minFee: number;
  maxFee: number;
}
