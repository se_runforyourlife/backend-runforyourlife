export enum Role {
  EventManager = 'EventManager',
  Runner = 'Runner',
  Admin = 'Admin',
}
