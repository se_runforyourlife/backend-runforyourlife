import { Entity, PrimaryColumn, Column, OneToMany } from 'typeorm';
import { Events } from './events';
import { RegistrationTransaction } from './RegistrationTransaction';
import { Notification } from './Notification';
import { Report } from './Report';
import { TransactionHistory } from './TransactionHistory';

@Entity()
export class User {
  @PrimaryColumn('varchar', { length: 255 })
  id!: string;

  @Column('varchar', { length: 255 })
  password!: string;

  @Column('varchar', { length: 255 })
  firstname!: string;

  @Column('varchar', { length: 255 })
  lastname!: string;

  @Column('varchar', { length: 255 })
  idCardNumber!: string;

  @Column('varchar', { length: 255 })
  phoneNumber!: string;

  @Column()
  birthDate!: Date;

  @Column('varchar', { length: 255 })
  gender!: string;

  @Column('varchar', { length: 255, nullable: true })
  bankAccount?: string;

  @Column('varchar', { length: 255, nullable: true })
  bankAccountNumber?: string;

  @Column('varchar', { length: 255 })
  role!: string;

  @Column('varchar', { length: 255, default: '' })
  imageURL!: string;

  @OneToMany(() => Events, event => event.owner, { cascade: true })
  events?: Events[];

  @OneToMany(() => RegistrationTransaction, transaction => transaction.runner, {
    cascade: true,
  })
  transactions?: RegistrationTransaction[];

  @OneToMany(() => TransactionHistory, history => history.runner, {
    cascade: true,
  })
  history?: TransactionHistory[];

  @OneToMany(() => Notification, notification => notification.to, {
    cascade: true,
  })
  notifications?: Notification[];

  @OneToMany(() => Notification, notification => notification.from, {
    cascade: true,
  })
  createdNotifications?: Notification[];

  @OneToMany(() => Report, report => report.reportTo, {
    cascade: true,
  })
  receivedReport?: Report[];

  @OneToMany(() => Report, report => report.reportFrom, {
    cascade: true,
  })
  sendedReport?: Report[];

  constructor(user?: Partial<User>) {
    if (user) {
      for (const key in user) {
        if (user.hasOwnProperty(key)) {
          this[key] = user[key];
        }
      }
    }
  }
}
