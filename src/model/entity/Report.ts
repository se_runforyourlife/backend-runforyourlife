import { Entity, PrimaryGeneratedColumn, ManyToOne, Column } from 'typeorm';
import { User } from './User';
import { Events } from './events';

@Entity()
export class Report {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column('varchar', { length: 255 })
  reason!: string;

  @ManyToOne(() => User, user => user.sendedReport)
  reportFrom?: User;

  @ManyToOne(() => User, user => user.receivedReport)
  reportTo?: User;

  @ManyToOne(() => Events)
  reportEvent?: Events;

  constructor(report?: Partial<Report>) {
    if (report) {
      for (const key in report) {
        if (report.hasOwnProperty(key)) {
          this[key] = report[key];
        }
      }
    }
  }
}
