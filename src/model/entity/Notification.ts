import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';
import { User } from './User';

@Entity()
export class Notification {
  @CreateDateColumn({ type: 'timestamp' })
  createdAt?: Date;
  @UpdateDateColumn({ type: 'timestamp' })
  updatedAt?: Date;
  @PrimaryGeneratedColumn()
  id!: number;
  @Column('varchar', { length: 255 })
  content!: string;
  @Column('varchar', { length: 255 })
  title!: string;
  @Column()
  isRead!: boolean;
  @Column()
  isHidden!: boolean;
  @ManyToOne(() => User, user => user.notifications)
  to?: User;
  @ManyToOne(() => User, user => user.createdNotifications)
  from?: User;
  constructor(notification?: Partial<Notification>) {
    if (notification) {
      for (const key in notification) {
        if (notification.hasOwnProperty(key)) {
          this[key] = notification[key];
        }
      }
    }
  }
}
