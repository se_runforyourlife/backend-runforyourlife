import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  ManyToOne,
} from 'typeorm';
import { RegistrationFee } from './RegistrationFee';
import { User } from './User';
import { RegistrationTransaction } from './RegistrationTransaction';
import { TransactionHistory } from './TransactionHistory';

@Entity()
export class Events {
  @PrimaryGeneratedColumn()
  id!: number;
  @Column('varchar', { length: 255 })
  name!: String;
  @Column('varchar', { length: 255, nullable: true })
  imageURL!: String;
  @Column('varchar', { length: 255 })
  type!: String;
  @Column()
  date!: Date;
  @Column()
  isRegister!: Boolean;
  @Column('varchar', { length: 255 })
  location!: String;
  @Column()
  minFee!: number;
  @Column()
  maxFee!: number;
  @Column('varchar', { length: 65535, nullable: true })
  detail?: string;
  @OneToMany(() => RegistrationFee, fee => fee.events, {
    cascade: true,
    onDelete: 'CASCADE',
  })
  feeList?: RegistrationFee[];
  @OneToMany(() => RegistrationTransaction, transaction => transaction.event, {
    cascade: true,
    onDelete: 'CASCADE',
  })
  transactions?: RegistrationTransaction[];
  @OneToMany(() => TransactionHistory, history => history.event, {
    cascade: true,
    onDelete: 'CASCADE',
  })
  history?: TransactionHistory[];
  @ManyToOne(() => User, user => user.events)
  owner?: User;
  constructor(event?: Partial<Events>) {
    if (event) {
      for (const key in event) {
        if (event.hasOwnProperty(key)) {
          this[key] = event[key];
        }
      }
    }
  }
}

@Entity()
export class TypeEvent {
  @PrimaryGeneratedColumn()
  id!: number;
  @Column()
  name!: String;
}
