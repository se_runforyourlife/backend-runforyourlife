import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity()
export class Test {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column('varchar', { length: 255, unique: true })
  content!: string;
}
