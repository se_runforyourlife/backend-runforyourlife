import {
  Entity,
  Column,
  ManyToOne,
  CreateDateColumn,
  UpdateDateColumn,
  PrimaryGeneratedColumn,
  OneToOne,
  JoinColumn,
} from 'typeorm';
import { RegistrationFee } from './RegistrationFee';
import { User } from './User';
import { Events } from './events';
import { RegistrationTransaction } from './RegistrationTransaction';

@Entity()
export class TransactionHistory {
  @PrimaryGeneratedColumn()
  id!: string;
  @CreateDateColumn({ type: 'timestamp' })
  createdAt?: Date;
  @UpdateDateColumn({ type: 'timestamp' })
  updatedAt?: Date;
  @Column()
  isPaid!: Boolean;
  @Column({ nullable: true })
  cancel!: Boolean;
  @Column('varchar', { length: 255 })
  method!: string;
  @Column('varchar', { length: 3, nullable: true })
  shirtSize!: string;
  @ManyToOne(() => Events, event => event.transactions, {
    nullable: false,
    onDelete: 'CASCADE',
  })
  event?: Events;
  @ManyToOne(() => User, user => user.transactions, {
    nullable: false,
    onDelete: 'CASCADE',
  })
  runner?: User;
  @ManyToOne(() => RegistrationFee, fee => fee.transactions, {
    onDelete: 'CASCADE',
  })
  fee?: RegistrationFee;
  @OneToOne(() => RegistrationTransaction, { nullable: true })
  @JoinColumn()
  transaction?: RegistrationTransaction;
  constructor(transaction?: Partial<TransactionHistory>) {
    if (transaction) {
      for (const key in transaction) {
        if (transaction.hasOwnProperty(key)) {
          this[key] = transaction[key];
        }
      }
    }
  }
}
