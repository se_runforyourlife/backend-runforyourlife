import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { Events } from './events';
import { RegistrationTransaction } from './RegistrationTransaction';
import { TransactionHistory } from './TransactionHistory';

@Entity()
export class RegistrationFee {
  @PrimaryGeneratedColumn()
  id!: number;

  @ManyToOne(() => Events, events => events.feeList, { onDelete: 'CASCADE' })
  events!: Events;

  @Column('varchar', { length: 255 })
  description!: string;

  @Column()
  price!: number;

  @OneToMany(() => RegistrationTransaction, transaction => transaction.fee, {
    cascade: true,
  })
  transactions?: RegistrationTransaction[];

  @OneToMany(() => TransactionHistory, history => history.fee, {
    cascade: true,
  })
  history?: TransactionHistory[];
  constructor(fee?: Partial<RegistrationFee>) {
    if (fee) {
      for (const key in fee) {
        if (fee.hasOwnProperty(key)) {
          this[key] = fee[key];
        }
      }
    }
  }
}
