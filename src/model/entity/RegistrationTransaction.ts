import {
  Entity,
  Column,
  ManyToOne,
  CreateDateColumn,
  UpdateDateColumn,
  OneToOne,
  JoinColumn,
} from 'typeorm';
import { RegistrationFee } from './RegistrationFee';
import { User } from './User';
import { Events } from './events';
import { TransactionHistory } from './TransactionHistory';

@Entity()
export class RegistrationTransaction {
  @CreateDateColumn({ type: 'timestamp' })
  createdAt?: Date;
  @UpdateDateColumn({ type: 'timestamp' })
  updatedAt?: Date;
  @Column()
  isPaid!: Boolean;
  @Column('varchar', { length: 255 })
  method!: string;
  @Column('varchar', { length: 3, nullable: true })
  shirtSize!: string;
  @ManyToOne(() => Events, event => event.transactions, {
    nullable: false,
    primary: true,
    onDelete: 'CASCADE',
  })
  event?: Events;
  @ManyToOne(() => User, user => user.transactions, {
    nullable: false,
    primary: true,
    onDelete: 'CASCADE',
  })
  runner?: User;
  @ManyToOne(() => RegistrationFee, fee => fee.transactions, {
    onDelete: 'CASCADE',
  })
  fee?: RegistrationFee;
  @OneToOne(() => TransactionHistory, { nullable: true })
  @JoinColumn()
  history?: TransactionHistory;
  constructor(transaction?: Partial<RegistrationTransaction>) {
    if (transaction) {
      for (const key in transaction) {
        if (transaction.hasOwnProperty(key)) {
          this[key] = transaction[key];
        }
      }
    }
  }
}
