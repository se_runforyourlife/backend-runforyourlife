import { User } from './entity/User';
import { sign, verify } from 'jsonwebtoken';
import { Role } from '../type/user-type';

export class JwtModel {
  public static generateJwt = (user: User, expiresIn = '7d') => {
    return {
      token: sign(
        { id: user.id, role: user.role },
        process.env.JWT_SECRET || 'secret',
        { expiresIn },
      ),
      refreshToken: sign({ id: user.id }, process.env.JWT_SECRET || 'secret'),
    };
  };
  public static verifyJwt = (token: string) => {
    return verify(token, process.env.JWT_SECRET || 'secret') as {
      id: string;
      role: Role;
    };
  };
}
