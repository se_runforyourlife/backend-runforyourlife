export default class ObjectModel {
  public static removeUndefined<T = object>(obj: T) {
    Object.keys(obj).forEach(key =>
      obj[key] === undefined ? delete obj[key] : '',
    );
    return obj;
  }
}
