export function controller(path = '') {
  return function<T extends { new (...args: any[]): {} }>(constructor: T) {
    return class extends constructor {
      basePath = path;
    };
  };
}
