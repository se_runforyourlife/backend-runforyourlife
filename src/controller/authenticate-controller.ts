import BaseController from './common/base-controller';
import { controller } from '../decorator/controller-decorator';
import { Context } from 'koa';
import { Route } from '../type';
import { getRepository } from 'typeorm';
import { User } from '../model/entity/User';
import { compareSync } from 'bcryptjs';
import stringValidator from '../middleware/validator/stringValidator';
import { JwtModel } from '../model/JwtModel';

@controller('/auth')
export default class AuthenticateController extends BaseController {
  private userRepository = getRepository(User);

  public postLogin = async (ctx: Context) => {
    const { id, password } = ctx.request.body;
    const user = await this.userRepository.findOne({ where: { id } });
    if (!user) {
      ctx.status = 403;
      return;
    }
    if (compareSync(password, user.password)) {
      ctx.body = JwtModel.generateJwt(user);
      ctx.status = 200;
      return;
    }
    ctx.status = 403;
    return;
  };

  protected getRoutes(): Route[] {
    return [
      {
        method: 'post',
        path: '/login',
        handler: this.postLogin,
        middlewares: [stringValidator('id'), stringValidator('password')],
      },
    ];
  }
}
