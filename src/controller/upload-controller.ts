import BaseController from './common/base-controller';
import { controller } from '../decorator/controller-decorator';
import { Context } from 'koa';
import { Route } from '../type';
import { getRepository } from 'typeorm';
import { Events } from '../model/entity/events';
import path from 'path';
import fs from 'fs';
import { User } from '../model/entity/User';

@controller('/upload')
export default class UploadController extends BaseController {
  private eventsRepository = getRepository(Events);
  private userRepository = getRepository(User);

  public postImage = async (ctx: Context) => {
    const { id } = ctx.params;
    const event = await this.eventsRepository.findOne(id, {
      relations: ['feeList'],
    });
    if (event !== undefined) {
      if (ctx.request && ctx.request.files) {
        console.log(ctx.request.files);
        const file = ctx.request.files.image;
        const filePath = file.path;
        const fileSection = file.name.split('.');
        const fileExt = fileSection[fileSection.length - 1];
        const saveName = 'banner_' + event.id.toString() + '.' + fileExt;
        const imagePath = path.join(
          __dirname + '../../../public/image',
          saveName,
        );
        const imageURL = process.env.BASE_NAME + '/image/' + saveName;
        fs.copyFileSync(filePath, imagePath);
        event.imageURL = imageURL;
        await this.eventsRepository.save(event);
        ctx.status = 200;
        ctx.body = { message: 'OK', URL: imageURL };
        return;
      } else {
        ctx.status = 500;
        ctx.body = { message: `Image not found` };
        console.log(ctx.request.files);
        return;
      }
    } else {
      ctx.status = 500;
      ctx.body = { message: `This event's id does not exist.` };
      return;
    }
  };

  public uploadProfile = async (ctx: Context) => {
    const { id } = ctx.params;
    const user = await this.userRepository.findOne(id);
    if (user !== undefined) {
      if (ctx.request && ctx.request.files) {
        console.log(ctx.request.files);
        const file = ctx.request.files.image;
        const filePath = file.path;
        const fileSection = file.name.split('.');
        const fileExt = fileSection[fileSection.length - 1];
        const saveName = `profile_${user.id}.${fileExt}`;
        const imagePath = path.join(
          `${__dirname}../../../public/image`,
          saveName,
        );
        const imageURL = `${process.env.BASE_NAME}/image/${saveName}`;
        fs.copyFileSync(filePath, imagePath);
        user.imageURL = imageURL;
        await this.userRepository.save(user);
        ctx.status = 200;
        ctx.body = { message: 'OK', URL: imageURL };
        return;
      } else {
        ctx.status = 500;
        ctx.body = { message: `Image not found` };
        console.log(ctx.request.files);
        return;
      }
    } else {
      ctx.status = 500;
      ctx.body = { message: `This event's id does not exist.` };
      return;
    }
  };

  protected getRoutes(): Route[] {
    return [
      {
        method: 'post',
        path: '/image/:id',
        handler: this.postImage,
      },
      {
        method: 'post',
        path: '/image/profile/:id',
        handler: this.uploadProfile,
      },
    ];
  }
}
