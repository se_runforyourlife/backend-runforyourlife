import BaseController from './common/base-controller';
import { controller } from '../decorator/controller-decorator';
import { Context } from 'koa';
import { Route, Event } from '../type';
import { Events, TypeEvent } from '../model/entity/events';
import { getRepository, getConnection } from 'typeorm';

@controller('/search')
export default class SearchController extends BaseController {
  public getEvents = async (ctx: Context) => {
    // get events
    const events = await getRepository(Events).find();
    console.log(events);
    ctx.body = events;
    ctx.status = 200;
    return;
  };
  public getTypeEvents = async (ctx: Context) => {
    const types = await getRepository(TypeEvent).find();
    console.log(types);
    ctx.body = types;
    ctx.status = 200;
  };
  public searchEvents = async (ctx: Context) => {
    // get events
    const events = await getRepository(Events).find();
    // read input
    const { search, filters } = ctx.request.body;
    console.log('request search ->', search, 'request filters -> ', filters);
    // เช็ค error
    // query from database
    // มีแต่ name or location
    let filterEvents: Event[] = [];
    if (!search && !filters.length) {
      filterEvents = events;
    } else if (search && !filters.length) {
      filterEvents = events.filter(
        e =>
          e.name.toLowerCase().includes(search.toLowerCase()) ||
          e.location.toLowerCase().includes(search.toLowerCase()),
      );
    }
    // only filter
    else if (!search && filters.length) {
      filterEvents = events.filter(e =>
        filters.some((f: any) => e.type.includes(f)),
      );
    }
    // have both
    else if (search && filters.length) {
      filterEvents = events.filter(
        e =>
          (e.name.toLowerCase().includes(search.toLowerCase()) ||
            e.location.toLowerCase().includes(search.toLowerCase())) &&
          filters.some((f: any) => e.type.includes(f)),
      );
    }
    // return event
    console.log('filterEvents', filterEvents);

    ctx.body = filterEvents;
    ctx.status = 200;
    return;
  };
  public insertEvents = (ctx: Context) => {
    // insert initial event
    const events = [
      {
        name: 'TU Law-Run',
        imageURL: 'https://sv1.picz.in.th/images/2019/01/31/TtDgqb.jpg',
        type: 'marathon',
        date: new Date('10 Mar 2019'),
        isRegister: true,
        location: 'Thammasat University, Bangkok',
        minFee: 500,
        maxFee: 1000,
      },
      {
        name: 'Run For Better City',
        imageURL: 'https://sv1.picz.in.th/images/2019/01/31/TtDwTy.jpg',
        type: 'half-marathon',
        date: new Date('15 Mar 2019'),
        isRegister: true,
        location: 'Chulalongkorn University, Bangkok',
        minFee: 800,
        maxFee: 2000,
      },
      {
        name: 'Sci KU Run 2019',
        imageURL: 'https://sv1.picz.in.th/images/2019/01/31/TtlVeN.jpg',
        type: 'mini-marathon',
        date: new Date('24 Mar 2019'),
        isRegister: true,
        location: 'Kasetsart University, Bangkok',
        minFee: 400,
        maxFee: 1500,
      },
      {
        name: 'Race on The Moon',
        imageURL: 'https://news.mthai.com/app/uploads/2018/10/1-1-1.jpg',
        type: 'micro-marathon',
        date: new Date('27 Jan 2019'),
        isRegister: true,
        location: 'Chanthaburi',
        minFee: 500,
        maxFee: 2000,
      },
      {
        name: 'TOOLGETHER RUN 2019',
        imageURL:
          'https://www.fanaticrun.com/assets/media/image/ToolGetherRun2019/Banner1_ToolGetherRun_2019_1000pix.jpeg',
        type: 'marathon',
        date: new Date('27 Jan 2019'),
        isRegister: true,
        location: 'Tambon Salaya, Amphoe Phutthamonthon, Nakhon Pathom',
        minFee: 400,
        maxFee: 1000,
      },
      {
        name: 'KMUTT MINI MARATHON',
        imageURL:
          'https://www.fanaticrun.com/assets/media/image/KMUTT-2019/KMUTT-RUN-2019_1000-1pix.jpg',
        type: 'mini-marathon',
        date: new Date('13 Jan 2019'),
        isRegister: true,
        location:
          "King Mongkut's University of Technology Thonburi, Bangkhuntien",
        minFee: 450,
        maxFee: 500,
      },
      {
        name: 'KHON KAEN INTERNATIONAL MARATHON 2019',
        imageURL: 'https://77kaoded.com/wp-content/uploads/Screenshot-408.png',
        type: 'marathon',
        date: new Date('3 AUG 2019'),
        isRegister: true,
        location: 'Khon Kaen',
        minFee: 640,
        maxFee: 1200,
      },
      {
        name: 'KORAT POWDURANCE MARATHON 2019',
        imageURL:
          'http://www.gotorace.com/wp-content/uploads/2018/10/Banner-KPM-2019.jpg',
        type: 'micro-marathon',
        date: new Date('10 MAR 2019'),
        isRegister: true,
        location:
          "His Majesty the King's 80th Birthday Anniversary, 5th December 2007 Sports Complex, Nakhon Ratchasima",
        minFee: 800,
        maxFee: 2400,
      },
    ];
    console.log('events ->', events);
    getConnection()
      .createQueryBuilder()
      .insert()
      .into('events')
      .values(events)
      .execute();
    ctx.body = `insert ${events.length} events to database!`;
    ctx.status = 200;
    return;
  };
  public insertTypeEvents = (ctx: Context) => {
    // insert initial type of event
    getConnection()
      .createQueryBuilder()
      .insert()
      .into('type_event')
      .values([
        {
          name: 'micro-marathon',
        },
        {
          name: 'mini-marathon',
        },
        {
          name: 'half-marathon',
        },
        {
          name: 'marathon',
        },
      ])
      .execute();
    ctx.body = 'insert type of events!';
    ctx.status = 200;
  };
  public deleteEvents = (ctx: Context) => {
    getConnection()
      .createQueryBuilder()
      .delete()
      .from(Events)
      .execute();
    ctx.body = 'Delete all events!';
    ctx.status = 200;
  };
  protected getRoutes(): Route[] {
    return [
      {
        method: 'get',
        path: '/events',
        handler: this.getEvents,
      },
      {
        method: 'post',
        path: '/results',
        handler: this.searchEvents,
      },
      {
        method: 'get',
        path: '/typeEvents',
        handler: this.getTypeEvents,
      },
      {
        method: 'get',
        path: '/insertEvents',
        handler: this.insertEvents,
      },
      {
        method: 'get',
        path: '/insertTypeEvents',
        handler: this.insertTypeEvents,
      },
      {
        method: 'get',
        path: '/deleteEvents',
        handler: this.deleteEvents,
      },
    ];
  }
}
