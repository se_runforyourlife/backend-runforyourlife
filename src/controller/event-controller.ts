import BaseController from './common/base-controller';
import { controller } from '../decorator/controller-decorator';
import { Context } from 'koa';
import { Route } from '../type';
import { getRepository } from 'typeorm';
import { Events } from '../model/entity/events';
import numberValidator from '../middleware/validator/numberValidator';
import stringValidator from '../middleware/validator/stringValidator';
import dateValidator from '../middleware/validator/dateValidator';
import booleanValidator from '../middleware/validator/booleanValidator';
import { RegistrationFee } from '../model/entity/RegistrationFee';
import { User } from '../model/entity/User';
import { Role } from '../type/user-type';
import authenticate from '../middleware/authenticate';

@controller('/event')
export default class EventController extends BaseController {
  private eventsRepository = getRepository(Events);
  private userRepository = getRepository(User);

  public getManyEvents = async (ctx: Context) => {
    const { eventManagerId, runnerId } = ctx.request.query;
    if (eventManagerId && runnerId) {
      ctx.status = 400;
      ctx.body = {
        message:
          'The query should be selected one of eventManagerId or runnerId.',
      };
      return;
    }
    if (eventManagerId) {
      const user = await this.userRepository.findOne(eventManagerId, {
        relations: ['events'],
      });
      if (user) {
        if (user.role !== Role.EventManager) {
          ctx.status = 404;
          ctx.body = { message: 'This eventManagerId does not exist.' };
          return;
        }
        ctx.status = 200;
        ctx.body = user.events;
        return;
      } else {
        ctx.status = 404;
        ctx.body = { message: 'This eventManagerId does not exist.' };
        return;
      }
    }
    if (runnerId) {
      const user = await this.userRepository.findOne(runnerId, {
        relations: ['transactions', 'transactions.event'],
      });
      if (user) {
        if (user.role !== Role.Runner) {
          ctx.status = 404;
          ctx.body = { message: 'This runner does not exist.' };
          return;
        }
        if (user.transactions) {
          ctx.status = 200;
          ctx.body = user.transactions.map(eachTransaction => {
            const { event, ...transaction } = eachTransaction;
            return {
              ...event,
              transaction,
            };
          });
          return;
        }
        ctx.status = 500;
        return;
      } else {
        ctx.status = 404;
        ctx.body = { message: 'This runner does not exist.' };
        return;
      }
    }
    const events = await this.eventsRepository.find({
      relations: ['owner', 'feeList'],
    });
    ctx.body = events;
    return;
  };

  public getEvent = async (ctx: Context) => {
    const { id } = ctx.params;
    const event = await this.eventsRepository.findOne(id, {
      relations: ['feeList'],
    });
    if (event !== undefined) {
      ctx.body = { ...event, detail: (event && event.detail) || '' };
      return;
    } else {
      ctx.status = 404;
      ctx.body = { message: `This event's id does not exist.` };
      return;
    }
  };

  public getOwner = async (ctx: Context) => {
    const { id } = ctx.params;
    const event = await this.eventsRepository.findOne(id, {
      relations: ['owner'],
    });
    if (!event) {
      ctx.status = 404;
      ctx.body = { message: `This event's id does not exist.` };
      return;
    }
    ctx.body = { owner: event.owner };
  };

  public addEvent = async (ctx: Context) => {
    const {
      name,
      imageURL,
      type,
      date,
      isRegister,
      location,
      minFee,
      maxFee,
      detail,
      feeList,
      ownerId,
    } = ctx.request.body;
    const owner = await this.userRepository.findOne(ownerId);
    let event: Events;
    if (owner) {
      if (owner.role === Role.EventManager) {
        event = new Events({
          name,
          imageURL,
          type,
          date,
          isRegister,
          location,
          minFee,
          maxFee,
          detail,
          feeList,
          owner,
        });
      } else {
        ctx.status = 400;
        ctx.body = { message: 'This eventManagerId does not exist.' };
        return;
      }
    } else {
      event = new Events({
        name,
        imageURL,
        type,
        date,
        isRegister,
        location,
        minFee,
        maxFee,
        detail,
        feeList,
      });
    }
    const addedEvent = await this.eventsRepository.save(event);
    ctx.body = { message: 'OK', id: addedEvent.id };
    return;
  };

  public editEvent = async (ctx: Context) => {
    const { id } = ctx.params;
    const event = await this.eventsRepository.findOne(id, {
      relations: ['feeList'],
    });
    console.log(ctx.request.body);
    if (event !== undefined) {
      for (const key in event) {
        console.log(key, ctx.request.body[key]);
        if (key !== 'feeList') {
          if (
            event.hasOwnProperty(key) &&
            ctx.request.body.hasOwnProperty(key)
          ) {
            event[key] = ctx.request.body[key];
          }
        }
      }
      if (ctx.request.body['feeList']) {
        const feeList = [];
        for (const fee of ctx.request.body['feeList']) {
          feeList.push(new RegistrationFee(fee));
        }
        event.feeList = feeList;
      }
      await this.eventsRepository.save(event);
      ctx.status = 200;
      ctx.body = { message: 'OK' };
    } else {
      ctx.status = 500;
      ctx.body = { message: `This event's id does not exist.` };
      return;
    }
  };

  public deleteEvent = async (ctx: Context) => {
    const { eventId } = ctx.params;
    await this.eventsRepository.delete(eventId);
    ctx.body = { message: 'OK' };
    return;
  };

  public changeEventStatus = async (ctx: Context) => {
    const { id } = ctx.params;
    const { isRegister } = ctx.request.body;
    const event = await this.eventsRepository.findOne(id, {
      relations: ['feeList'],
    });
    if (!event) {
      ctx.status = 400;
      return (ctx.body = { message: `This event's id does not exist.` });
    }
    console.log(isRegister);
    event.isRegister = isRegister;
    await this.eventsRepository.save(event);
    return (ctx.body = { message: 'OK' });
  };

  public addMockDetail = async (ctx: Context) => {
    const details: string[] = [
      `วิ่งที่ไหนเมื่อไหร่ก็ได้ สะสมระยะ 10 กม. (ไม่จำเป็นต้องวิ่งจบในครั้งเดียว) ภายในเดือนมีนาคม 2562 (ตั้งแต่ วันที่ 1 มีนาคม - 31 มีนาคม 2562) โดยภายหลังจากการสมัครท่านสามารถเดิน - วิ่งสะสมระยะ หรือใช้ผลการเข้าร่วมการแข่งขันรายการวิ่งต่าง ๆ เป็นระยะสะสมก็สามารถทำได้ และส่งผลได้ทั้งทาง wong.konkanok@gmail.com หรือ www.Facebook/ป่ะไปวิ่ง โดยการส่งผลวิ่ง นักวิ่งสามารถใช้ Application ที่สามารถจับระยะทาง วิ่งได้เช่น Endomondo, Garmin Amazfit, Nike, strava หรือถ่ายรูปจาก Treadmill ก็ได้ และส่งผลรวมพร้อมกันภายในวันที่ 1 มีนาคม - 5 เมษายน 2562 เท่านั้น`,
      `ไม่ว่าคุณจะเป็นมือใหม่ หรือมือเก๋าก็สามารถร่วมท้าทายเป้าหมายได้ วิ่งสะสมระยะ 10 กม. ภายในวันที่ 15 มีนาคม 2562 วิ่งที่ไหนเมื่อไหร่ก็ได้ ส่งผลการวิ่งเพื่อพิชิตรางวัลแห่งความภูมิใจ เราหวังเป็นอย่างยิ่งที่จะทำให้คุณและคนที่คุณรักมีเป้าหมายในการวิ่งที่สร้างคุณค่าได้มากยิ่งขึ้น นอกจากจะได้สุขภาพที่ดี คุณยังได้ร่วมแบ่งปันสิ่งดีๆ ให้กับสังคม โดยรายได้ส่วนหนึ่งมอบองค์กรการกุศลที่ให้ความช่วยเหลือสุนัขในประเทศไทย`,
      `วัตถุประสงค์ เพื่อฉลองครบรอบ 50 ปี ของบริษัท โตชิบา ไทยแลนด์ จำกัด สนับสนุนให้ประชาชนและคนรุ่นใหม่หันมาสนใจในการกีฬาให้มากขึ้น นอกจากนี้ยังเป็นการช่วยส่งเสริมธุรกิจด้านการท่องเที่ยวเชิงกีฬา (Sport Tourism) ก่อให้เกิดรายได้สู่ท้องถิ่น โดยตั้งเป้าหมายว่า เงินรายได้หักค่าใช้จ่ายเพื่อมอบให้โรงพยาบาลในท้องถิ่น ของจันทบุรีมอบให้ศูนย์ความเป็นเลิศด้านมะเร็ง รพ.พระปกเกล้า จันทบุรี`,
    ];
    ctx.request.body.detail =
      details[Math.floor(Math.random() * details.length)];
    const base = Math.floor(Math.random() * 10) * 100;
    const pricing = [
      { description: 'Normal', price: base },
      { description: 'VIP', price: base + 200 },
      { description: 'SUPER VIP', price: base + 1200 },
      { description: 'SUPER SAIYAN VIP', price: base + 2000 },
      { description: 'SUPER SAIYAN GOD VIP', price: base + 10000 },
    ];
    ctx.request.body.feeList = pricing;
    await this.editEvent(ctx);
  };

  protected getRoutes(): Route[] {
    return [
      {
        method: 'get',
        path: '/',
        handler: this.getManyEvents,
        middlewares: [
          stringValidator('eventManagerId', { isOptional: true }, 'query'),
          stringValidator('runnerId', { isOptional: true }, 'query'),
        ],
      },
      {
        method: 'get',
        path: '/:id',
        handler: this.getEvent,
        middlewares: [numberValidator('id', { min: 1 }, 'params')],
      },
      {
        method: 'get',
        path: '/:id/owner',
        handler: this.getOwner,
        middlewares: [numberValidator('id', { min: 1 }, 'params')],
      },
      {
        method: 'patch',
        path: '/:id',
        handler: this.editEvent,
        middlewares: [
          numberValidator('id', { min: 1 }, 'params'),
          numberValidator('id', { min: 1, isOptional: true }),
          stringValidator('name', { isOptional: true }),
          stringValidator('imageURL', { isOptional: true }),
          stringValidator('type', { isOptional: true }),
          dateValidator('date', { isOptional: true }),
          booleanValidator('isRegister', { isOptional: true }),
          stringValidator('location', { isOptional: true }),
          numberValidator('minFee', { isOptional: true }),
          numberValidator('maxFee', { isOptional: true }),
          stringValidator('detail', { isOptional: true }),
        ],
      },
      {
        method: 'patch',
        path: '/:id/mock',
        handler: this.addMockDetail,
        middlewares: [
          numberValidator('id', { min: 1 }, 'params'),
          stringValidator('name', { isOptional: true }),
          stringValidator('imageURL', { isOptional: true }),
          stringValidator('type', { isOptional: true }),
          dateValidator('date', { isOptional: true }),
          booleanValidator('isRegister', { isOptional: true }),
          stringValidator('location', { isOptional: true }),
          numberValidator('minFee', { isOptional: true }),
          numberValidator('maxFee', { isOptional: true }),
          stringValidator('detail', { isOptional: true }),
        ],
      },
      {
        method: 'post',
        path: '/',
        handler: this.addEvent,
        middlewares: [
          stringValidator('name'),
          stringValidator('imageURL', { isOptional: true }),
          stringValidator('type'),
          dateValidator('date'),
          booleanValidator('isRegister'),
          stringValidator('location'),
          numberValidator('minFee'),
          numberValidator('maxFee'),
          stringValidator('detail', { isOptional: true }),
          stringValidator('ownerId', { isOptional: true }),
        ],
      },
      {
        method: 'delete',
        path: '/:eventId',
        handler: this.deleteEvent,
        middlewares: [
          authenticate([Role.Admin, Role.EventManager]),
          numberValidator('eventId', {}, 'params'),
        ],
      },
      {
        method: 'patch',
        path: '/:id/status',
        handler: this.changeEventStatus,
        middlewares: [
          authenticate([Role.Admin]),
          booleanValidator('isRegister'),
        ],
      },
    ];
  }
}
