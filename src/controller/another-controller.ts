import BaseController from './common/base-controller';
import { Route } from '../type';
import { Context } from 'koa';
// import { getRepository } from 'typeorm';
// import { Test } from '../model/entity/test';
import { controller } from '../decorator/controller-decorator';

@controller('/another')
export default class AnotherController extends BaseController {
  // private testRepository = getRepository(Test);

  public getAll = async (ctx: Context) => {
    // ctx.body = await this.testRepository.find();
  };

  public create = async (ctx: Context) => {
    const { content } = ctx.request.body;
    if (!content) {
      ctx.status = 400;
      return;
    }
    // const test = new Test();
    // test.content = content;
    // await this.testRepository.save(test);
    // ctx.body = await this.testRepository.findOne({ content: test.content });
  };

  protected getRoutes(): Route[] {
    return [
      {
        method: 'get',
        path: '/',
        handler: this.getAll,
      },
      {
        method: 'post',
        path: '/',
        handler: this.create,
      },
    ];
  }
}
