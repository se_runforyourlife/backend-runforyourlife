import BaseController from './common/base-controller';
import { Route } from '../type';
import { Context } from 'koa';
import { getRepository } from 'typeorm';
import { Test } from '../model/entity/test';
import { controller } from '../decorator/controller-decorator';
import { Report } from '../model/entity/Report';
import stringValidator from '../middleware/validator/stringValidator';
import authenticate from '../middleware/authenticate';
import { Events } from '../model/entity/events';

@controller('/report')
export default class ReportController extends BaseController {
  private reportRepository = getRepository(Report);
  private eventsRepository = getRepository(Events);

  public getAllReport = async (ctx: Context) => {
    ctx.body = await this.reportRepository.find({
      relations: ['reportFrom', 'reportTo', 'reportEvent'],
    });
    return;
  };

  public createReport = async (ctx: Context) => {
    const {
      reason,
      reportedId: reportTo,
      reportedEvent: reportEvent,
    } = ctx.request.body;
    const { id: reportFrom, role } = ctx.user;
    const event = await this.eventsRepository.findOne(reportEvent, {
      relations: ['owner'],
    });
    if (!event) {
      ctx.status = 400;
      return (ctx.body = { message: `Event ${reportEvent} does not exist.` });
    }
    if (!event.owner || event.owner.id !== reportTo) {
      ctx.status = 400;
      return (ctx.body = {
        message: `Event ${reportEvent} does not own by event manager ${reportTo}.`,
      });
    }
    const report = new Report({ reason, reportFrom, reportTo, reportEvent });
    await this.reportRepository.save(report);
    ctx.body = { message: 'OK' };
    return;
  };

  protected getRoutes(): Route[] {
    return [
      {
        method: 'get',
        path: '/',
        handler: this.getAllReport,
      },
      {
        method: 'post',
        path: '/',
        handler: this.createReport,
        middlewares: [
          authenticate(),
          stringValidator('reason'),
          stringValidator('reportedId'),
        ],
      },
    ];
  }
}
