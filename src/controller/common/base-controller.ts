import Router from 'koa-router';
import { Route } from '../../type';

export default abstract class BaseController {
  readonly router = new Router();
  protected basePath?: string;

  public mountToRouter(parentRouter: Router): void {
    this.getRoutes().forEach(({ path, method, handler, middlewares }) => {
      this.router[method](path, ...(middlewares || []), handler);
    });
    parentRouter.use(this.basePath || '', this.router.routes());
  }

  public mountToController(parentController: BaseController): void {
    this.mountToRouter(parentController.router);
  }

  protected abstract getRoutes(): Route[];
}
