import BaseController from './common/base-controller';
import { Route } from '../type';
import { Context } from 'koa';
import { getRepository } from 'typeorm';
import { controller } from '../decorator/controller-decorator';
import { RegistrationTransaction } from '../model/entity/RegistrationTransaction';
import numberValidator from '../middleware/validator/numberValidator';
import authenticate from '../middleware/authenticate';
import { Role } from '../type/user-type';
import stringValidator from '../middleware/validator/stringValidator';
import { RegistrationFee } from '../model/entity/RegistrationFee';
import ObjectModel from '../model/ObjectModel';
import { TransactionHistory } from '../model/entity/TransactionHistory';
import { Events } from '../model/entity/events';

@controller('/transaction')
export default class TransactionController extends BaseController {
  private transactionRepository = getRepository(RegistrationTransaction);
  private feeRepository = getRepository(RegistrationFee);
  private historyRepository = getRepository(TransactionHistory);
  private eventRepository = getRepository(Events);
  public getAllTransactions = async (ctx: Context) => {
    const {
      runnerId: runner,
      eventId: event,
      eventManagerId,
    } = ctx.request.query;
    const relations = ['event', 'runner', 'fee', 'history'];
    if (eventManagerId) {
      relations.push('event.owner');
    }
    const where = { event, runner };
    ObjectModel.removeUndefined(where);
    let transaction = await this.transactionRepository.find({
      relations,
      where,
    });
    if (eventManagerId) {
      transaction = transaction.filter(
        e => e.event && e.event.owner && e.event.owner.id === eventManagerId,
      );
    }
    ctx.body = transaction;
    return;
  };
  public getHistory = async (ctx: Context) => {
    const { runnerId: runner, eventId: event } = ctx.request.query;
    const relations = ['event', 'runner', 'fee'];
    const where = { event, runner };
    ObjectModel.removeUndefined(where);
    ctx.body = await this.historyRepository.find({ relations, where });
    return;
  };
  public reserveEvent = async (ctx: Context) => {
    const { eventId } = ctx.params;
    const { id: runnerId, role } = ctx.user;
    const { feeId, method, shirtSize } = ctx.request.body;
    if (role !== Role.Runner) {
      ctx.status = 401;
      ctx.body = { message: 'This user is not a runner' };
      return;
    }
    const [oldTransaction, fee] = await Promise.all([
      this.transactionRepository.findOne({
        event: eventId,
        runner: runnerId,
      }),
      this.feeRepository.findOne({ id: feeId }, { relations: ['events'] }),
    ]);
    if (oldTransaction) {
      ctx.status = 409;
      return;
    }
    if (fee && fee.events.id !== eventId) {
      ctx.status = 400;
      ctx.body = { message: 'feeId does not relate with eventId ' };
      return;
    }
    const history = new TransactionHistory({
      method,
      shirtSize,
      runner: runnerId,
      event: eventId,
      fee: feeId,
      isPaid: false,
    });
    const savedHistory = await this.historyRepository.save(history);
    const transaction = new RegistrationTransaction({
      method,
      shirtSize,
      runner: runnerId,
      event: eventId,
      fee: feeId,
      isPaid: false,
      history: savedHistory,
    });
    await this.transactionRepository.save(transaction);
    ctx.status = 200;
    ctx.body = { message: 'OK' };
    return;
  };

  public pay = async (ctx: Context) => {
    const { eventId } = ctx.params;
    const { id: runnerId } = ctx.user;
    const relations = ['event', 'runner', 'history'];
    const transaction = await this.transactionRepository.findOne(
      {
        runner: runnerId,
        event: eventId,
      },
      { relations },
    );
    if (transaction) {
      if (transaction.isPaid) {
        ctx.status = 400;
        ctx.body = { message: 'This transaction was paid' };
        return;
      }
      const { history } = transaction;
      if (history) {
        history.isPaid = true;
        await this.historyRepository.save(history);
      }
      transaction.isPaid = true;
      await this.transactionRepository.save(transaction);
      ctx.status = 200;
      ctx.body = { message: 'OK' };
      return;
    } else {
      ctx.status = 404;
      ctx.body = {
        message: 'There is no reservation for this event and this runner',
      };
      return;
    }
  };

  public cancel = async (ctx: Context) => {
    const { eventId } = ctx.params;
    const { id: runnerId } = ctx.user;
    await this.deleteTransaction(eventId, runnerId);
    ctx.status = 200;
    return;
  };

  public disjoin = async (ctx: Context) => {
    const { eventId, runnerId } = ctx.params;
    const { id: eventManagerId, role } = ctx.user;
    const event = await this.eventRepository.findOne(eventId, {
      relations: ['owner'],
    });
    if (!event) {
      ctx.status = 400;
      ctx.body = { message: 'This eventId is invalid.' };
      return;
    }
    if (
      role === Role.Admin ||
      (event && event.owner && event.owner.id === eventManagerId)
    ) {
      await this.deleteTransaction(eventId, runnerId);
    } else {
      ctx.status = 401;
      ctx.body = { message: 'This user has no permission to request disjoin.' };
      return;
    }
    ctx.status = 200;
    return;
  };

  private deleteTransaction = async (eventId: number, runnerId: string) => {
    const relations = ['event', 'runner', 'history'];
    const transaction = await this.transactionRepository.findOne({
      relations,
      where: { event: eventId, runner: runnerId },
    });
    if (transaction) {
      const { history } = transaction;
      if (history) {
        history.cancel = true;
        await this.historyRepository.save(history);
      }
      await this.transactionRepository.delete({
        runner: runnerId,
        event: eventId,
      } as any);
    }
  };

  protected getRoutes(): Route[] {
    return [
      {
        method: 'get',
        path: '/',
        handler: this.getAllTransactions,
        middlewares: [
          stringValidator('runnerId', { isOptional: true }, 'query'),
          numberValidator('eventId', { isOptional: true }, 'query'),
          stringValidator('eventManagerId', { isOptional: true }, 'query'),
        ],
      },
      {
        method: 'get',
        path: '/history',
        handler: this.getHistory,
        middlewares: [
          stringValidator('runnerId', { isOptional: true }, 'query'),
          numberValidator('eventId', { isOptional: true }, 'query'),
        ],
      },
      {
        method: 'post',
        path: '/reserve/:eventId',
        handler: this.reserveEvent,
        middlewares: [
          authenticate([Role.Runner]),
          numberValidator('eventId', {}, 'params'),
          stringValidator('method'),
          numberValidator('feeId'),
        ],
      },
      {
        method: 'post',
        path: '/pay/:eventId',
        handler: this.pay,
        middlewares: [
          authenticate([Role.Runner]),
          numberValidator('eventId', {}, 'params'),
        ],
      },
      {
        method: 'post',
        path: '/cancel/:eventId',
        handler: this.cancel,
        middlewares: [
          authenticate([Role.Runner]),
          numberValidator('eventId', {}, 'params'),
        ],
      },
      {
        method: 'post',
        path: '/disjoin/:eventId/:runnerId',
        handler: this.disjoin,
        middlewares: [
          authenticate([Role.EventManager, Role.Admin]),
          numberValidator('eventId', {}, 'params'),
          stringValidator('runnerId', {}, 'params'),
        ],
      },
    ];
  }
}
