import BaseController from './common/base-controller';
import { Route } from '../type';
import { Context } from 'koa';
import { getRepository, FindManyOptions } from 'typeorm';
import { controller } from '../decorator/controller-decorator';
import stringValidator from '../middleware/validator/stringValidator';
``;
import authenticate from '../middleware/authenticate';
import numberValidator from '../middleware/validator/numberValidator';
import { Notification } from '../model/entity/Notification';
import ObjectModel from '../model/ObjectModel';
import booleanValidator from '../middleware/validator/booleanValidator';
import { User } from '../model/entity/User';

@controller('/notification')
export default class NotificationController extends BaseController {
  private notificationRepository = getRepository(Notification);
  private userRepository = getRepository(User);
  public getNotifications = async (ctx: Context) => {
    const { id: to } = ctx.user;
    const { limit } = ctx.request.query;
    const notifications = await this.notificationRepository.find(
      ObjectModel.removeUndefined<FindManyOptions<Notification>>({
        where: { to },
        relations: ['to', 'from'],
        order: { updatedAt: 'DESC' },
        take: limit,
      }),
    );
    ctx.body = notifications;
    return;
  };

  public createNotification = async (ctx: Context) => {
    const { receiverId: to, content, title, isHidden } = ctx.request.body;
    const { id: from } = ctx.user;
    const user = await this.userRepository.findOne(to);
    if (!user) {
      ctx.status = 404;
      ctx.body = { message: 'There is no receiver' };
      return;
    }
    const notification = new Notification({
      content,
      to,
      from,
      title,
      isHidden: isHidden || false,
      isRead: false,
    });
    await this.notificationRepository.save(notification);
    ctx.body = { message: 'OK' };
    return;
  };

  public readNotification = async (ctx: Context) => {
    const { id } = ctx.user;
    const { notificaitonId } = ctx.params;
    const notification = await this.notificationRepository.findOne(
      notificaitonId,
    );
    if (notification) {
      if (notification.to !== id) {
        ctx.status = 401;
        return;
      }
      notification.isRead = true;
      await this.notificationRepository.save(notification);
      ctx.body = { message: 'OK' };
      return;
    } else {
      ctx.status = 404;
      return;
    }
  };

  public deleteNotification = async (ctx: Context) => {
    const { notificationId } = ctx.params;
    await this.notificationRepository.delete(notificationId);
    ctx.body = { message: 'OK' };
    return;
  };

  protected getRoutes(): Route[] {
    return [
      {
        method: 'get',
        path: '/',
        handler: this.getNotifications,
        middlewares: [
          authenticate(),
          numberValidator('limit', { isOptional: true }, 'query'),
        ],
      },
      {
        method: 'post',
        path: '/',
        handler: this.createNotification,
        middlewares: [
          authenticate(),
          stringValidator('receiverId'),
          stringValidator('content'),
          stringValidator('title'),
          booleanValidator('isHidden'),
        ],
      },
      {
        method: 'post',
        path: '/:notificationId',
        handler: this.readNotification,
        middlewares: [
          authenticate(),
          numberValidator('notificationId', {}, 'params'),
        ],
      },
      {
        method: 'delete',
        path: '/:notificationId',
        handler: this.deleteNotification,
        middlewares: [numberValidator('notificationId', {}, 'params')],
      },
    ];
  }
}
