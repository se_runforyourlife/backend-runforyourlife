import BaseController from './common/base-controller';
import { controller } from '../decorator/controller-decorator';
import { Context } from 'koa';
import { Route } from '../type';
import { getRepository } from 'typeorm';
import { User } from '../model/entity/User';
import { genSaltSync, hashSync } from 'bcryptjs';
import { Role } from '../type/user-type';
import stringValidator from '../middleware/validator/stringValidator';
import dateValidator from '../middleware/validator/dateValidator';
import authenticate from '../middleware/authenticate';

@controller('/user')
export default class UserController extends BaseController {
  private userRepository = getRepository(User);
  public getUsers = async (ctx: Context) => {
    try {
      const users = await this.userRepository.find();
      ctx.body = { users };
      ctx.status = 200;
      return;
    } catch (error) {
      ctx.body = { message: error.toString() };
      ctx.status = 500;
      return;
    }
  };

  public getUserById = async (ctx: Context) => {
    const { id } = ctx.request.body;
    try {
      const user = await this.userRepository.findOne({ where: { id } });

      ctx.body = { ...user };
      ctx.status = 200;
      return;
    } catch (error) {
      ctx.body = { message: error.toString() };
      ctx.status = 500;
      return;
    }
  };

  public updateRunner = async (ctx: Context) => {
    const {
      id,
      firstname,
      lastname,
      idCardNumber,
      phoneNumber,
      birthDate,
      gender,
    } = ctx.request.body;

    const user: User | undefined = await this.userRepository.findOne({
      where: { id },
    });
    if (user) {
      user.id = id || user.id;
      user.firstname = firstname || user.firstname;
      user.lastname = lastname || user.lastname;
      user.idCardNumber = idCardNumber || user.idCardNumber;
      user.phoneNumber = phoneNumber || user.phoneNumber;
      user.birthDate = birthDate || user.birthDate;
      user.gender = gender || user.gender;
      await this.userRepository.update({ id }, user);
      user.password = '';
      ctx.status = 200;
      ctx.body = { message: user };
      return;
    } else {
      ctx.status = 500;
      return;
    }
  };
  public createEventManager = async (ctx: Context) => {
    const {
      id,
      password,
      firstname,
      lastname,
      idCardNumber,
      phoneNumber,
      birthDate,
      gender,
      bankAccount,
      bankAccountNumber,
    } = ctx.request.body;
    const oldUser = await this.userRepository.findOne({ id });
    if (oldUser) {
      ctx.status = 409;
      ctx.body = { message: 'This id already exists.' };
      return;
    }
    const user = new User({
      id,
      firstname,
      lastname,
      idCardNumber,
      phoneNumber,
      birthDate,
      gender,
      bankAccount,
      bankAccountNumber,
      password: hashSync(password, genSaltSync()),
      role: Role.EventManager,
    });

    await this.userRepository.save(user);

    ctx.status = 200;
    ctx.body = { message: 'Success' };
    return;
  };

  public createRunner = async (ctx: Context) => {
    const {
      id,
      password,
      firstname,
      lastname,
      idCardNumber,
      phoneNumber,
      birthDate,
      gender,
    } = ctx.request.body;
    const oldUser = await this.userRepository.findOne({ id });
    if (oldUser) {
      ctx.status = 409;
      ctx.body = { message: 'This id already exists.' };
      return;
    }
    const user = new User({
      id,
      firstname,
      lastname,
      idCardNumber,
      phoneNumber,
      birthDate,
      gender,
      password: hashSync(password, genSaltSync()),
      role: Role.Runner,
    });

    await this.userRepository.save(user);

    ctx.status = 200;
    ctx.body = { message: 'Success' };
    return;
  };

  public createAdmin = async (ctx: Context) => {
    const {
      id,
      password,
      firstname,
      lastname,
      idCardNumber,
      phoneNumber,
      birthDate,
      gender,
    } = ctx.request.body;
    const oldUser = await this.userRepository.findOne(id);
    if (oldUser) {
      ctx.status = 409;
      ctx.body = { message: 'This id already exists.' };
      return;
    }
    const user = new User({
      id,
      firstname,
      lastname,
      idCardNumber,
      phoneNumber,
      birthDate,
      gender,
      password: hashSync(password, genSaltSync()),
      role: Role.Admin,
    });

    await this.userRepository.save(user);

    ctx.status = 200;
    ctx.body = { message: 'Success' };
    return;
  };

  protected getRoutes(): Route[] {
    return [
      {
        method: 'get',
        path: '/',
        handler: this.getUsers,
        middlewares: [authenticate()],
      },
      {
        method: 'post',
        path: '/update-runner',
        handler: this.updateRunner,
        // middlewares: [authenticate()]
      },
      {
        method: 'post',
        path: '/userid',
        handler: this.getUserById,
      },
      {
        method: 'post',
        path: '/event-manager',
        handler: this.createEventManager,
        middlewares: [
          stringValidator('id', { maxLength: 255 }),
          stringValidator('password', { maxLength: 255 }),
          stringValidator('firstname', { maxLength: 255 }),
          stringValidator('lastname', { maxLength: 255 }),
          stringValidator('idCardNumber', {
            pattern: /^[0-9]*$/,
            minLength: 13,
            maxLength: 13,
          }),
          stringValidator('phoneNumber', { pattern: /^[0-9]*$/ }),
          dateValidator('birthDate'),
          stringValidator('gender'),
          stringValidator('bankAccount'),
          stringValidator('bankAccountNumber', {
            pattern: /^[0-9]*$/,
          }),
        ],
      },
      {
        method: 'post',
        path: '/runner',
        handler: this.createRunner,
        middlewares: [
          stringValidator('id', { maxLength: 255 }),
          stringValidator('password', { maxLength: 255 }),
          stringValidator('firstname', { maxLength: 255 }),
          stringValidator('lastname', { maxLength: 255 }),
          stringValidator('idCardNumber', {
            pattern: /^[0-9]*$/,
            minLength: 13,
            maxLength: 13,
          }),
          stringValidator('phoneNumber', { pattern: /^[0-9]*$/ }),
          dateValidator('birthDate'),
          stringValidator('gender'),
        ],
      },
      {
        method: 'post',
        path: '/admin',
        handler: this.createAdmin,
        middlewares: [
          stringValidator('id', { maxLength: 255 }),
          stringValidator('password', { maxLength: 255 }),
          stringValidator('firstname', { maxLength: 255 }),
          stringValidator('lastname', { maxLength: 255 }),
          stringValidator('idCardNumber', {
            pattern: /^[0-9]*$/,
            minLength: 13,
            maxLength: 13,
          }),
          stringValidator('phoneNumber', { pattern: /^[0-9]*$/ }),
          dateValidator('birthDate'),
          stringValidator('gender'),
        ],
      },
    ];
  }
}
