import BaseController from './common/base-controller';
import { controller } from '../decorator/controller-decorator';
import { Context } from 'koa';
import { Route } from '../type';

@controller('/')
export default class RootController extends BaseController {
  public index = async (ctx: Context) => {
    ctx.body = {
      message: 'Hello Run for your life!',
    };
  };
  protected getRoutes(): Route[] {
    return [
      {
        method: 'get',
        path: '/',
        handler: this.index,
      },
    ];
  }
}
