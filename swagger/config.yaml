openapi: 3.0.0
info:
  version: "1.0.0"
  title: "Backend Run For Your Life"
  description: "API Docs for SE Project `Run For Your Life`"
host: "localhost:3000"
paths:
  /event:
    get:
      tags:
      - "event"
      summary: "Get all events. US6-4"
      description: ""
      operationId: "getAllEvents"
      produces:
      - "application/json"
      parameters:
      - in: "query"
        name: "eventManagerId"
        description: "Get all events which are owned by this event manager."
        require: false
        schema:
          type: "string"
      - in: "query"
        name: "runnerId"
        description: "Get all events which this runner registered."
        require: false
        schema:
          type: "string"
      responses:
        200:
          description: "successful operation"
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/definitions/Event'
  /transaction:
    get:
      tags:
      - "transaction"
      summary: "Get all transaction. US1-1 US6-1 US6-6"
      description: ""
      operationId: "getAllTransactions"
      produces:
      - "application/json"
      parameters:
      - in: "query"
        name: "eventId"
        description: "Get all reserved and paid transactions of the event."
        require: false
        schema:
          type: "string"
      - in: "query"
        name: "runnerId"
        description: "Get all reserved and paid transactions of the runner."
        require: false
        schema:
          type: "string"
      responses:
        200:
          description: "successful operation"
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/definitions/Transaction'
  /transaction/reserve/{eventId}:
    post:
      tags:
      - transaction
      summary: "Reserve an event. US5-3"
      description: "Create a transaction to reserve event, login as runner is required."
      operationId: "reserveEvent"
      produces:
      - "application/json"
      parameters:
      - in: "param"
        name: "eventId"
        description: "Reserved event."
        require: true
        schema:
          type: "integer"
          format: "int32"
      - in: "body"
        name: "method"
        description: "Method to pay."
        require: true
        schema:
          type: "string"
      - in: "body"
        name: "feeId"
        description: "Event fee."
        require: true
        schema:
          type: "integer"
          format: "int32"
      responses:
        200:
          description: "successful operation"
          content:
            application/json:
              schema:
                type: object
                properties:
                  message:
                    type: string
                    example: OK
  /transaction/pay/{eventId}:
    post:
      tags:
      - transaction
      summary: "Pay for a reservation. US1-2"
      description: "Change transaction state to be paid, login as runner is required."
      operationId: "payEvent"
      produces:
      - "application/json"
      parameters:
      - in: "param"
        name: "eventId"
        description: "Paid event."
        require: true
        schema:
          type: "integer"
          format: "int32"
      responses:
        200:
          description: "successful operation"
          content:
            application/json:
              schema:
                type: object
                properties:
                  message:
                    type: string
                    example: OK
  /transaction/cancel/{eventId}:
    post:
      tags:
      - transaction
      summary: "Cancel paid or reserved event. US5-1"
      description: "Cancel paid or reserved event and remove transaction from the database, login as runner is required."
      operationId: "cancelEvent"
      produces:
      - "application/json"
      parameters:
      - in: "param"
        name: "eventId"
        description: "Cancelled event."
        require: true
        schema:
          type: "integer"
          format: "int32"
      responses:
        200:
          description: "successful operation"
          content:
            application/json:
              schema:
                type: object
                properties:
                  message:
                    type: string
                    example: OK
  /transaction/disjoin/{eventId}/{runnerId}:
    post:
      tags:
      - transaction
      summary: "Cancel paid or reserved event. US5-2"
      description: "Cancel paid or reserved event and remove transaction from the database, login as event manager or admin is required."
      operationId: "disjoinEvent"
      produces:
      - "application/json"
      parameters:
      - in: "param"
        name: "eventId"
        description: "Cancelled event."
        require: true
        schema:
          type: "integer"
          format: "int32"
      - in: "param"
        name: "runnerId"
        description: "Cancelled runner."
        require: true
        schema:
          type: "integer"
          format: "int32"
      responses:
        200:
          description: "successful operation"
          content:
            application/json:
              schema:
                type: object
                properties:
                  message:
                    type: string
                    example: OK
  /report:
    get:
      tags:
      - "report"
      summary: "Get all reports. US4-3"
      description: ""
      operationId: "getAllReports"
      produces:
      - "application/json"
      responses:
        200:
          description: "successful operation"
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/definitions/Report'
    post:
      tags:
      - "report"
      summary: "Send report. US4-3"
      description: "Report a user, login is required."
      operationId: "createReport"
      produces:
      - "application/json"
      parameters:
      - in: "body"
        name: "reason"
        description: "Report reason."
        require: true
        schema:
          type: "string"
      - in: "body"
        name: "reportedId"
        description: "Reported user."
        require: true
        schema:
          type: "string"
      responses:
        200:
          description: "successful operation"
          content:
            application/json:
              schema:
                type: object
                properties:
                  message:
                    type: string
                    example: OK
  /notification:
    get:
      tags:
      - "notification"
      summary: "Get user's notifications. US8-1"
      description: "Get newest user notifications, login is required."
      operationId: "getNotifications"
      produces:
      - "application/json"
      parameters:
      - in: "query"
        name: "limit"
        description: "limit number of notifications."
        require: false
        schema:
          type: "integer"
      responses:
        200:
          description: "successful operation"
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/definitions/Notification'
    post:
      tags:
      - "notification"
      summary: "Create a notification US8-1"
      description: "Create a notification in the database, login is required."
      operationId: "createNotification"
      produces:
      - "application/json"
      parameters:
      - in: "body"
        name: "receiverId"
        description: "Received user id."
        require: true
        schema:
          type: "string"
      - in: "body"
        name: "content"
        description: "Notification content."
        require: true
        schema:
          type: "string"
      - in: "body"
        name: "title"
        description: "Notification title."
        require: true
        schema:
          type: "string"
      - in: "body"
        name: "isHidden"
        description: "Notification visibility."
        require: true
        schema:
          type: "boolean"
      responses:
        200:
          description: "successful operation"
          content:
            application/json:
              schema:
                type: object
                properties:
                  message:
                    type: string
                    example: OK
  /notification/{notificationId}:
    post:
      tags:
      - "notification"
      summary: "Read a notification. US8-1"
      description: "Update a notification to be read, login is required."
      operationId: "readNotification"
      produces:
      - "application/json"
      parameters:
      - in: "param"
        name: "notificaitonId"
        description: "Read notification."
        require: true
        schema:
          type: "integer"
      responses:
        200:
          description: "successful operation"
          content:
            application/json:
              schema:
                type: object
                properties:
                  message:
                    type: string
                    example: OK
    delete:
      tags:
      - "notification"
      summary: "Delete a notification. US8-1"
      description: "Delete a notification."
      operationId: "deleteNotification"
      produces:
      - "application/json"
      parameters:
      - in: "param"
        name: "notificaitonId"
        description: "Deleted notification."
        require: true
        schema:
          type: "integer"
      responses:
        200:
          description: "successful operation"
          content:
            application/json:
              schema:
                type: object
                properties:
                  message:
                    type: string
                    example: OK
    
definitions:
  Event:
    type: "object"
    properties:
      id:
        type: "integer"
        format: "int32"
      name:
        type: "string"
      imageURL:
        type: "string"
      date:
        type: "string"
        format: "date-time"
      isRegister:
        type: boolean
      location:
        type: string
      minFee:
        type: "integer"
        format: "int32"
      maxFee:
        type: "integer"
        format: "int32"
      detail:
        type: string
      owner:
        $ref: '#/definitions/UserRef'
      feeList:
        $ref: '#/definitions/RegistrationFeeRef'
  Report:
    type: object
    properties:
      id:
        type: integer
        format: int32
      reason:
        type: string
      reportFrom:
        $ref: '#/definitions/UserRef'
      reportTo:
        $ref: '#/definitions/UserRef'
  EventRef:
    type: "object"
    properties:
      id:
        type: "integer"
        format: "int32"
      name:
        type: "string"
      imageURL:
        type: "string"
      date:
        type: "string"
        format: "date-time"
      isRegister:
        type: boolean
      location:
        type: string
      minFee:
        type: "integer"
        format: "int32"
      maxFee:
        type: "integer"
        format: "int32"
      detail:
        type: string
  UserRef:
    type: "object"
    properties:
      id:
        type: string
      password:
        type: string
      firstname:
        type: string
      lastname:
        type: string
      idCardNumber:
        type: string
      phoneNumber:
        type: string
      birthDate:
        type: string
        format: date-time
      gender:
        type: string
      bankAccount:
        type: string
      bankAccountNumber:
        type: string
      role:
        type: string
  RegistrationFeeRef:
    type: object
    properties:
      id:
        type: integer
      description:
        type: string
      price:
        type: integer
  Transaction:
    type: object
    properties:
      id:
        type: string
      createdAt:
        type: string
        format: date-time
      updatedAt:
        type: string
        format: date-time
      isPaid:
        type: boolean
      cancel:
        type: boolean
      method:
        type: string
      event:
        $ref: '#/definitions/EventRef'
      runner:
        $ref: '#/definitions/UserRef'
      fee:
        $ref: '#/definitions/RegistrationFeeRef'
  Notification:
    type: object
    properties:
      id:
        type: integer
      createdAt:
        type: string
        format: date-time
      updatedAt:
        type: string
        format: date-time
      content:
        type: string
      title:
        type: string
      isRead:
        type: boolean
      isHidden:
        type: boolean
      from:
        $ref: '#/definitions/UserRef'
      to:
        $ref: '#/definitions/UserRef'
