# Coding Guideline

-----

## Project Structure

All Source Code is in the folder src with these structure

```
src
├── app.ts
├── config
│   └── index.ts
├── controller
│   ├── another-controller.ts
│   ├── common
│   │   └── base-controller.ts
│   └── root-controller.ts
├── decorator
│   ├── controller-decorator.ts
│   └── index.ts
├── model
│   ├── entity
│   │   └── test.ts
│   ├── migration
│   └── subscriber
├── router.ts
└── type
    ├── controller-type.ts
    └── index.ts
```

1. app.ts contain the main application
2. router.ts contain application routing configuration
3. config folder contain all the configurable values for the project
4. controller folder contain all of the application controller
5. decorator folder contain all of the application decorator
6. model folder contain all of the application model with
   1. subfolder entity contain TypeORM's Entity
   2. subfolder migration contain TypeORM's SQL Migration Script
   3. subfolder subscriber contain TypeORM's subscription
7. type folder contain type definition and interface for the application

## Mounting controller to main router

create a controller and mount it to router in file "router.ts"

```ts
// an example of mounting controller to the router
new SomeController().mountToRouter(router);

// You can also mount controller to the controller
new SomeController().mountToController(controller);
```

## Writing a controller

Create a file {name}-controller.ts inside controller folder or subfolder of controller folder

Example for controller implementation

```ts
// Define controller base path using Controller Decorator
@controller('/basePath')
export default class ExampleController extends BaseController {
  // get Entity's Repository for Database CRUD
  private entityRepository = getRepository(Entity)

  // Define handler as a controller method
  public get = async (ctx: Context) => {
    ctx.body = await entityRepository.find();
  }

  public create = async (ctx: Context) => {
    const { x, y } = ctx.request.body;
    const entity = new Entity();
    entity.x = x;
    entity.y = y;
    await entityRepository.save(entity);
    ctx.body = 'OK';
  }

  // Implement getRoutes method
  protected getRoutes(): Route[] {
    /**
    * You have to return an array of routes
    * A router contain of 4 parts: method, path, handler and middlewares
    **/
    return [
      {
        path: '/', // Path to join with base route
        method: 'get', // get, post, put, patch and delete available
        handler: this.get
      },
      {
        path: '/',
        method: 'post',
        handler: this.create,
        // Define middleware in an array
        middlewares: [
          middleware1,
          middleware2
        ]
      }
    ];
  }
}
```

## Database CRUD

Using TypeORM as an ORM for PostgreSQL: http://typeorm.io

### Practice for using TypeORM for CRUD

1. Using TypeORM's repository for simple operation (CRUD with/without join, condition, etc.): http://typeorm.io/#/working-with-repository
2. For complex querying try to use query builder first (http://typeorm.io/#/select-query-builder) then try raw query API from the repository

## Testing

Using Jest for testing: https://jestjs.io

All test file are in \__tests__ folder. Test files are named {name}.spec.ts or {name}.test.ts


