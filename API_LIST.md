# API LIST
```typescript
interface Event {
    id: number;
    name: string;
    imageURL: string;
    type: string;
    date: Date;
    isRegister: boolean;
    location: string;
    minFee: number;
    maxFee: number;
    detail: string;
    feeList: RegistrationFee[];
}
interface RegistrationFee {
    id: number;
    description: string;
    price: number;
}
```
## GET /event
    Get all events
* Query: eventManagerId (optional)  
    Get all events of this event manager

## GET /event/:eventId
    Get an event which match eventId  
    Return Event
## POST /event
    Create an event  
    Body Event  
    Return { message: 'OK' , id: addedEventId }
## PATCH /event/:eventId
    Update an event which match eventId  
    Body Partial < Event >  
    Return { message: 'OK' }
## PATCH /event/:eventId/mock
    Update an event to have a mock data in detail and registration fee
    Return { message: 'OK' }
## POST /upload/image/:eventId
    upload the banner image of specified event
Body {image=<image_file>}
    Return { message: 'OK', URL: imageURL}