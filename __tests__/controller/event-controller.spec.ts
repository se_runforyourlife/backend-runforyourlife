import { createMockContext } from '@shopify/jest-koa-mocks';
import EventController from '../../src/controller/event-controller';
import { createConnection, getRepository, getConnection } from 'typeorm';
import { Events } from '../../src/model/entity/events';
import { User } from '../../src/model/entity/User';
import { RegistrationFee } from '../../src/model/entity/RegistrationFee';
import { Role } from '../../src/type/user-type';

describe('Get Many Events', () => {
  beforeAll(async () => {
    await createConnection({
      type: 'sqlite',
      database: ':memory:',
      entities: [Events, User, RegistrationFee],
      cache: true,
      synchronize: true,
    });
    const userRepository = getRepository(User);
    await userRepository.save([
      {
        id: 'runner',
        password: '',
        firstname: 'test',
        lastname: 'test',
        idCardNumber: '1234567890123',
        phoneNumber: '1234567890',
        birthDate: new Date(),
        gender: 'male',
        bankAccount: 'bank',
        bankAccountNumber: '1234567890',
        role: Role.Runner,
      },
      {
        id: 'manager',
        password: '',
        firstname: 'test',
        lastname: 'test',
        idCardNumber: '1234567890123',
        phoneNumber: '1234567890',
        birthDate: new Date(),
        gender: 'male',
        bankAccount: 'bank',
        bankAccountNumber: '1234567890',
        role: Role.EventManager,
      },
    ]);
  });

  it('Cannot select both runnerId and eventId', async () => {
    const ctx = createMockContext({
      url: '/?eventManagerId=1&&runnerId=2',
    });
    const controller = new EventController();
    await controller.getManyEvents(ctx);
    expect(ctx.body.message).toEqual(
      'The query should be selected one of eventManagerId or runnerId.',
    );
    expect(ctx.status).toEqual(400);
  });

  it('Runner is not event manager', async () => {
    const ctx = createMockContext({
      url: '/?eventManagerId=runner',
    });
    const controller = new EventController();
    await controller.getManyEvents(ctx);
    expect(ctx.body.message).toEqual('This eventManagerId does not exist.');
    expect(ctx.status).toEqual(400);
  });

  it('Event manager not found', async () => {
    const ctx = createMockContext({
      url: '/?eventManagerId=notId',
    });
    const controller = new EventController();
    await controller.getManyEvents(ctx);
    expect(ctx.body.message).toEqual('This eventManagerId does not exist.');
    expect(ctx.status).toEqual(404);
  });

  it('Event manager found and return result', async () => {
    const ctx = createMockContext({
      url: '/?eventManagerId=manager',
    });
    const controller = new EventController();
    await controller.getManyEvents(ctx);
    expect(ctx.body).toEqual([]);
    expect(ctx.status).toEqual(200);
  });

  afterAll(async () => {
    const connection = getConnection();
    await connection.close();
  });
});

describe('Get event by id', () => {
  beforeAll(async () => {
    await createConnection({
      type: 'sqlite',
      database: ':memory:',
      entities: [Events, User, RegistrationFee],
      cache: true,
      synchronize: true,
    });
    const userRepository = getRepository(User);
    await userRepository.save({
      id: 'manager',
      password: '',
      firstname: 'test',
      lastname: 'test',
      idCardNumber: '1234567890123',
      phoneNumber: '1234567890',
      birthDate: new Date(),
      gender: 'male',
      bankAccount: 'bank',
      bankAccountNumber: '1234567890',
      role: Role.EventManager,
    });
    const eventRepository = getRepository(Events);
    const event = new Events({
      name: 'event',
      type: 'marathon',
      date: new Date(),
      isRegister: true,
      location: 'location',
      detail: 'detail',
      minFee: 0,
      maxFee: 0,
      feeList: [
        new RegistrationFee({
          description: 'fee',
          price: 0,
        }),
      ],
    });
    await eventRepository.save(event);
  });

  it('Cannot find event', async () => {
    const ctx = createMockContext();
    ctx.params = {
      id: -1,
    };
    const controller = new EventController();
    await controller.getEvent(ctx);
    expect(ctx.status).toEqual(404);
    expect(ctx.body.message).toEqual(`This event's id does not exist.`);
  });

  it('Can find event', async () => {
    const eventRepository = getRepository(Events);
    const event = await eventRepository.findOne();
    const ctx = createMockContext();
    ctx.params = {
      id: event!.id,
    };
    const controller = new EventController();
    await controller.getEvent(ctx);
    expect(ctx.status).toEqual(200);
    expect(ctx.body.id).toEqual(event!.id);
  });

  afterAll(async () => {
    const connection = await getConnection();
    await connection.close();
  });
});
